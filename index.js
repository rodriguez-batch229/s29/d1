db.product.insertMany([
    {
        name: "Iphone X",
        price: 30000,
        isActive: true,
    },
    {
        name: "Samsung Galaxy S21",
        price: 51000,
        isActive: true,
    },
    {
        name: "Razer Blackshark V2X",
        price: 2800,
        isActive: false,
    },
    {
        name: "RAKK Gaming Mouse",
        price: 1800,
        isActive: true,
    },
    {
        name: "Razer Mechanical Keyboard",
        price: 4000,
        isActive: true,
    },
]);

// Query Operators
// Allow us to expand our queries and define conditions instead of just looking for specific values

// $gt - greater than

db.products.find({ price: { $gt: 3000 } });

// $lt - less than

db.products.find({ price: { $lt: 3000 } });

// $gte - greater than or equal to

db.products.find({ price: { $gte: 3000 } });

// $lte - less than or equal to
db.products.find({ price: { $lte: 2800 } });

users = [
    /* 1 */
    {
        _id: ObjectId("63bfafd38e80b6b2207d992c"),
        firstName: "Mary Jane",
        lastName: "Watson",
        email: "mjtiger@gmail.com",
        password: "tigerjackpot15",
        isAdmin: false,
    },

    /* 2 */
    {
        _id: ObjectId("63bfafd38e80b6b2207d992d"),
        firstName: "Gwen",
        lastName: "Stacy",
        email: "stacyTech@gmail.com",
        password: "stacyTech1991",
        isAdmin: true,
    },

    /* 3 */
    {
        _id: ObjectId("63bfafd38e80b6b2207d992e"),
        firstName: "Peter",
        lastName: "Parker",
        email: "peterWebDev@gmail.com",
        password: "webdeveloperpeter",
        isAdmin: true,
    },

    /* 4 */
    {
        _id: ObjectId("63bfafd38e80b6b2207d992f"),
        firstName: "Joan",
        lastName: "Jameson",
        email: "jjjameson@gmail.com",
        password: "spideyisamenace",
        isAdmin: false,
    },

    /* 5 */
    {
        _id: ObjectId("63bfafd38e80b6b2207d9930"),
        firstName: "Otto",
        lastName: "Octavius",
        email: "ottoOctopi@gmail.com",
        password: "docOck25",
        isAdmin: true,
    },
];

// $regex - query operator which allow us to find documents which will match character we ara looking for.

// $regex to look for documents with a partial match and by default is case sensitive

db.users.find({ firstName: { $regex: "e" } });

// $options: $i - will make our regex case insensitive

db.users.find({ firstName: { $regex: "O", $options: "$i" } });

// We can also use $regex for partial matches

db.products.find({ name: { $regex: "phone", $options: "$i" } });
db.users.find({ email: { $regex: "web", $options: "$i" } });
db.products.find({ name: { $regex: "razer", $options: "$i" } });
db.products.find({ name: { $regex: "rakk", $options: "$i" } });

// $or $and - logical operators and they work almost the same as in JSON
//  $or - allow us to set a condition to find for document which can satisfy at least 1 of the given condition

// db.products.find({$or: [{name: {$regex: 'x',$options: '$i'}},{price:{$lte:10000}}]})

db.products.find({
    $or: [
        {
            name: { $regex: "x", $options: "$i" },
        },
        {
            price: { $gte: 30000 },
        },
    ],
});

// $and - look or find documents that satisfies both conditions
db.products.find({
    $and: [
        {
            name: { $regex: "razer", $options: "$i" },
        },
        {
            price: { $gte: 3000 },
        },
    ],
});

db.users.find({
    $and: [
        {
            lastName: { $regex: "a", $options: "$i" },
        },
        {
            isAdmin: true,
        },
    ],
});

// Field Projection - allows us to hide/show certain field and properties of documents that were retrieved

db.users.find({}, { _id: 0, password: 0 });

db.users.find({ isAdmin: true }, { _id: 0, email: 1 });

//field projection = 0 means hide, 1 means show
//by default, we have explicity hide the id field

db.users.find({ isAdmin: false }, { firstName: 1, lastName: 1 }); //will still show _id
